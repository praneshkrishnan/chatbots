# Chatbots

This project lists down all the possible chatbot tools. Please look around to explore all documentation and links to developing and deploying chatbots. 

References

- [Rasa: Open source conversational AI](https://rasa.com/)
- [Google Dialog Flow](https://cloud.google.com/dialogflow/docs)
- [Amazon Lex](https://aws.amazon.com/lex/)
- [IBM Chatbot](https://www.ibm.com/products/watson-assistant/docs-resources)
- [Oracle Digital Assitant](https://www.oracle.com/chatbots/)
- [Microsoft Azure Bot](https://azure.microsoft.com/en-us/services/bot-services/)

Opensource
- [Microsoft Bot Framework](https://github.com/microsoft/botframework-sdk)
- [Botkit](https://github.com/howdyai/botkit-cms)
- [Botpress](https://botpress.com/)
- [RASA](https://github.com/RasaHQ/rasa)
- [Wit ai](https://github.com/wit-ai)
- [OpenDialog](https://opendialog.ai/)
- [Botonic](https://github.com/hubtype/botonic)
- [Cloudia Bot Builder](https://github.com/claudiajs/claudia-bot-builder)
- [Tock](https://github.com/theopenconversationkit/tock)
- [Botman](https://github.com/botman/botman)
- [Bottender](https://github.com/yoctol/bottender)
- [DeepPavlov](https://github.com/deepmipt/deeppavlov)
- [Golem](https://github.com/prihoda/golem)
- []()
- []()
- []()
- []()
- []()
- []()
- []()
- []()


NLU Libraries
- []()
- []()
- []()
- []()
- []()
- []()

